import pygame
import random
from time import sleep
from time import time
import sys
import numpy as np
pygame.init()
pygame.font.init()

width, height = 1491, 805
window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Sorting")

render = False
hud = False

array_len = 0

block_height = 0
block_width = 0
block_margin = 0
def update(data):
    if render:
        window.fill((15,15,15))
        margin = 2

        for d in data:
            pygame.draw.rect(
                window,
                (240,240,240),
                pygame.Rect(margin, height + 1 - d * block_height, block_width, d * block_height)
            )
            margin += block_margin
        pygame.display.update()

def bubblesort(data):
    fully_sorted = False
    sorted = False
    i = 0
    while not fully_sorted:

        if i + 1 == len(data):
            if not sorted:
                fully_sorted = True
            else:
                sorted = False
                i = 0
        if not fully_sorted:
            if data[i] > data[i + 1]:
                sorted = True
                data[i], data[i + 1] = data[i + 1], data[i]
                update(data)
            i += 1

def selectionsort(data):
    fully_sorted = False
    sorted = False
    i = 0
    while not fully_sorted:
        if i + 1 == len(data):
            if not sorted:
                fully_sorted = True
            else:
                sorted = False
                i = 0

        if not fully_sorted:
            p = i + 1
            m = i
            while p < len(data):
                if data[p] < data[m]:
                    m = p
                p += 1
            if not m == i:
                sorted = True
                data[i], data[m] = data[m], data[i]
                update(data)
            i += 1

def shellsort(data):

    n = len(data)
    gap = n//2

    while gap > 0:

        for i in range(gap,n):

            temp = data[i]

            j = i
            while  j >= gap and data[j-gap] >temp:
                data[j] = data[j-gap]
                update(data)
                j -= gap

            data[j] = temp
            update(data)
        gap //= 2

def heapify(data, n, i):
    largest = i
    l = 2 * i + 1
    r = 2 * i + 2

    if l < n and data[i] < data[l]:
        largest = l

    if r < n and data[largest] < data[r]:
        largest = r

    if largest != i:
        (data[i], data[largest]) = (data[largest], data[i])
        update(data)

        heapify(data, n, largest)

def heapsort(data):
    n = len(data)

    for i in range(n // 2 - 1, -1, -1):
        heapify(data, n, i)

    for i in range(n - 1, 0, -1):
        data[i], data[0] = data[0], data[i]
        update(data)
        heapify(data, i, 0)

def cocktailshaker(data):
    i = 0
    min_i = -1
    max_i = len(data) - 1
    up = True
    sorted = False
    fully_sorted = False
    while not fully_sorted:

        if up:
            if not i + 1 == max_i:
                i += 1
            else:
                if not sorted:
                    fully_sorted = True
                else:
                    sorted = False
                i -= 1
                up = False
                max_i -= 1
        elif not up:
            if not i - 1 == min_i:
                i -= 1
            else:
                if not sorted:
                    fully_sorted = True
                else:
                    sorted = False
                i += 1
                up = True
                min_i += 1

        if not fully_sorted:
            if data[i] > data[i + 1]:
                data[i], data[i + 1] = data[i + 1], data[i]
                sorted = True
                update(data)

def insertionsort(data):
    i = 1
    while not i == len(data):
        j = i
        while j > 0:
            if data[j] < data[j - 1]:
                data[j], data[j - 1] = data[j - 1], data[j]
                update(data)
            j -= 1

        i += 1

def partition(data, l, h):

    pivot = data[h]

    i = l - 1

    for j in range(l, h):
        if data[j] <= pivot:

            i = i + 1

            data[i], data[j] = data[j], data[i]
            update(data)

    data[i + 1], data[h] = data[h], data[i + 1]
    update(data)

    return i + 1

def merge(data, l, m, r):
    n1 = m - l + 1
    n2 = r - m

    L = [0] * (n1)
    R = [0] * (n2)

    for i in range(0, n1):
        L[i] = data[l + i]

    for j in range(0, n2):
        R[j] = data[m + 1 + j]

    i = 0
    j = 0
    k = l

    while i < n1 and j < n2:
        if L[i] <= R[j]:
            data[k] = L[i]
            update(data)
            i += 1
        else:
            data[k] = R[j]
            update(data)
            j += 1
        k += 1

    while i < n1:
        data[k] = L[i]
        update(data)
        i += 1
        k += 1

    while j < n2:
        data[k] = R[j]
        update(data)
        j += 1
        k += 1

def mergesort(data, l, r):
    if l < r:

        m = l+(r-l)//2

        mergesort(data, l, m)
        mergesort(data, m+1, r)
        merge(data, l, m, r)

def quicksort(data, l, h):
    if l < h:
        pivot = partition(data, l, h)
        quicksort(data, l, pivot - 1)
        quicksort(data, pivot + 1, h)

def bogosort(data):
    fully_sorted = False
    i = 0
    s = False
    update(data)
    while not fully_sorted:

        if not data[i] < data[i + 1]:
            s = True
        else:
            i += 1

        if s:
            i = 0
            s = False
            random.shuffle(data)
            update(data)
        elif not s and i >= len(data) - 1:
            fully_sorted = True

def main():
    global block_height, block_margin, block_width, array_len, height, window

    if "pygame" in sys.argv or "window" in sys.argv:
        global render
        render = True

    if str(1000) in sys.argv:
        height = 1005
        window = pygame.display.set_mode((width, height))
        block_height = 1
        block_width = 2
        block_margin = 1.488
        array_len = 1000
        data = random.sample(range(1,1000), 999)
    elif str(500) in sys.argv:
        block_height = 1.6
        block_width = 3
        block_margin = 2.98
        array_len = 500
        data = random.sample(range(1,500), 499)
    elif str(200) in sys.argv:
        block_height = 4
        block_width = 8
        block_margin = 7.47
        array_len = 200
        data = random.sample(range(1,200), 199)
    elif str(100) in sys.argv:
        block_height = 8
        block_width = 12
        block_margin = 15
        array_len = 100
        data = random.sample(range(1,100), 99)
    elif str(75) in sys.argv:
        block_height = 11
        block_width = 17
        block_margin = 20
        array_len = 75
        data = random.sample(range(1,75), 74)
    elif str(50) in sys.argv:
        block_height = 16
        block_width = 23
        block_margin = 30.5
        array_len = 50
        data = random.sample(range(1,50), 49)
    elif str(25) in sys.argv:
        block_height = 32
        block_width = 53
        block_margin = 63
        array_len = 25
        data = random.sample(range(1,25), 24)
    elif str(10) in sys.argv:
        block_height = 70
        block_width = 130
        block_margin = 150
        array_len = 11
        data = random.sample(range(1,11), 10)
    else:
        block_height = 8
        block_width = 12
        block_margin = 15
        array_len = 100
        data = random.sample(range(1,100), 99)

    if str(1) in sys.argv or "bubble" in sys.argv:
        start_time = time()
        bubblesort(data)
        print(time() - start_time)
    elif str(2) in sys.argv or "insertion" in sys.argv:
        start_time = time()
        insertionsort(data)
        print(time() - start_time)
    elif str(7) in sys.argv or "quick" in sys.argv:
        start_time = time()
        quicksort(data, 0, len(data) - 1)
        print(time() - start_time)
    elif str(0) in sys.argv or "bogo" in sys.argv:
        start_time = time()
        bogosort(data)
        print(time() - start_time)
    elif str(3) in sys.argv or "cocktailshaker" in sys.argv:
        start_time = time()
        cocktailshaker(data)
        print(time() - start_time)
    elif str(4) in sys.argv or "selection" in sys.argv:
        start_time = time()
        selectionsort(data)
        print(time() - start_time)
    elif str(6) in sys.argv or "merge" in sys.argv:
        start_time = time()
        mergesort(data, 0, len(data) - 1)
        print(time() - start_time)
    elif str(5) in sys.argv or "heap" in sys.argv:
        start_time = time()
        heapsort(data)
        print(time() - start_time)
    elif str(8) in sys.argv or "shell" in sys.argv:
        start_time = time()
        shellsort(data)
        print(time() - start_time)

    sleep(3)

if __name__ == "__main__":
    main()
